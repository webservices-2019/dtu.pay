#!/bin/bash
export PATH=/usr/local/share/dotnet:$PATH

dotnet build
xargs -n1 -r0a <(find . -type f -iname '*.Tests.csproj' -print0) dotnet test --no-build
docker-compose -f docker-compose.yml build
docker-compose -f docker-compose.yml up -d --remove-orphans
sleep 10
dotnet test ./DTU.Pay.SystemTests/DTU.Pay.SystemTests.csproj
OUT=$?
docker-compose -f docker-compose.yml down

if [ $OUT -eq 0 ];then
    exit 0
else
   exit $OUT
fi