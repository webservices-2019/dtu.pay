﻿// ***********************************************************************
// Assembly         : CustomerClient
// Author           : Kris (Lasse)
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="CustomerClient.cs" company="DTU">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using DTU.Pay.API.Model;
using DTU.Pay.Domain;
using JetBrains.Annotations;
using RestSharp;

namespace DTU.Pay.Clients
{
    [PublicAPI]
    public class CustomerClient : RestClientBase
    {
        public async Task<TokensModel> CreateTokensAsync(CPRNumber cpr, uint amount)
        {
            RestRequest restRequest = new RestRequest("/tokens", Method.POST);
            restRequest.AddParameter("cpr", cpr.ToString());
            restRequest.AddParameter("amount", amount.ToString());

            return await ExecuteAsync<TokensModel>(restRequest);
        }

        public async Task<List<TokenModel>> GetTokensAsync(CPRNumber cpr)
        {
            RestRequest restRequest = new RestRequest("/users/{cpr}/tokens", Method.GET);
            restRequest.AddUrlSegment("cpr", cpr.Value);
            return await ExecuteAsync<List<TokenModel>>(restRequest);
        }

        public async Task CreateUserAsync(User user, decimal balance)
        {
            RestRequest restRequest = new RestRequest("/users", Method.POST);
            restRequest.AddParameter("cpr", user.Cpr.ToString());
            restRequest.AddParameter("fName", user.FirstName);
            restRequest.AddParameter("lName", user.LastName);
            restRequest.AddParameter("balanceM", balance.ToString(CultureInfo.InvariantCulture));

            await _client.ExecutePostTaskAsync(restRequest);
        }

        public async void DeleteUserAsync(User user)
        {
            RestRequest restRequest = new RestRequest("/users", Method.DELETE);
            restRequest.AddParameter("cpr", user.Cpr.ToString());

            await _client.ExecutePostTaskAsync(restRequest);
        }

        /// <inheritdoc />
        public CustomerClient(string baseUrl) : base(baseUrl) { }
    }
}