﻿// ***********************************************************************
// Assembly         : RestClientBase
// Author           : Lasse
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="RestClientBase.cs" company="RestClientBase">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Serialization.Json;

namespace DTU.Pay.Clients
{
    public class RestClientBase
    {
        protected readonly RestClient _client;

        protected RestClientBase(string baseUrl)
        {
            _client = new RestClient();
            _client.Timeout = 5000;
            _client.BaseUrl = new Uri(baseUrl);
            _client.AddHandler("application/json", NewtonsoftJsonSerializer.Default);
            _client.AddHandler("text/json", NewtonsoftJsonSerializer.Default);
            _client.AddHandler("text/x-json", NewtonsoftJsonSerializer.Default);
            _client.AddHandler("text/javascript", NewtonsoftJsonSerializer.Default);
            _client.AddHandler("*+json", NewtonsoftJsonSerializer.Default);
        }

        protected async Task<T> ExecuteAsync<T>(IRestRequest request)
        {
            IRestResponse<T> response = await _client.ExecuteTaskAsync<T>(request);

            if (response.ErrorException == null)
                return response.Data;

            const string message = "Error retrieving response.  Check inner details for more info.";
            ApplicationException applicationException = new ApplicationException(message, response.ErrorException);
            throw applicationException;
        }
    }
}