﻿// ***********************************************************************
// Assembly         : MerchantClient
// Author           : Lasse
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="MerchantClient.cs" company="DTU">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Globalization;
using System.Threading.Tasks;
using DTU.Pay.Domain;
using JetBrains.Annotations;
using RestSharp;

namespace DTU.Pay.Clients
{
    [PublicAPI]
    public class MerchantClient : RestClientBase
    {


        public async Task<Order> CreateOrderAsync(CPRNumber merchantCprNumber, TokenID tokenId, string description, decimal price)
        {
            RestRequest restRequest = new RestRequest("/orders", Method.POST);
            restRequest.AddParameter("tokenID", tokenId.Value);
            restRequest.AddParameter("price", price);
            restRequest.AddParameter("merchCPR", merchantCprNumber.Value);
            restRequest.AddParameter("desc", description);
            return await ExecuteAsync<Order>(restRequest);
        }

        public async Task<ReportModelRest> GetReportAsync(CPRNumber cpr)
        {
            RestRequest restRequest = new RestRequest("/reports", Method.GET);
            restRequest.AddQueryParameter("cpr", cpr.Value);
            return await ExecuteAsync<ReportModelRest>(restRequest);
        }

        public async Task<bool> CreateUserAsync(User user, decimal balance)
        {
            RestRequest restRequest = new RestRequest("/users", Method.POST);
            restRequest.AddParameter("cpr", user.Cpr.ToString());
            restRequest.AddParameter("fName", user.FirstName);
            restRequest.AddParameter("lName", user.LastName);
            restRequest.AddParameter("balanceM", balance.ToString(CultureInfo.InvariantCulture));

            IRestResponse foo = await _client.ExecutePostTaskAsync(restRequest);
            return foo.IsSuccessful;
        }

        /// <inheritdoc />
        public MerchantClient(string baseUrl) : base(baseUrl) { }
    }
}