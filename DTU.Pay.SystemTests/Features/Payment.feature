﻿#Author: Mads
Feature: Payment

Scenario: Simple payment
	Given a clean bank
	And a registered customer with one unused token 
	And a registered merchant with a bank account
	And the customer has a bank account
	When the merchant scans the token
	And requests payment for 10 kroner using the token
	Then the payment succeeds
	And the money is transferred from the customer bank account to the merchant bank account

Scenario: Insufficient funds
	Given a clean bank
	And a registered customer with one unused token 
	And a registered merchant with a bank account
	And the customer has a bank account
	When the merchant scans the token
	And requests payment for 50000 kroner using the token
	Then the payment fails

Scenario: Refund
	Given a clean bank
	And a registered customer with one unused token 
	And a registered merchant with a bank account
	And the customer has a bank account
	When the merchant scans the token
	And requests payment for 100 kroner using the token
	And the payment succeeds
	And the money is transferred from the customer bank account to the merchant bank account 
	Then the customer requests a refund
	And the money is transferred from the merchant bank account to the customer bank account 

Scenario: Customer requests orders
	Given a clean bank
	And a registered customer with one unused token 
	And a registered merchant with a bank account
	And the customer has a bank account
	When the merchant scans the token
	And requests payment for 10 kroner using the token
	And the payment succeeds
	And the money is transferred from the customer bank account to the merchant bank account
	Then the customer requests a list of his orders
	And the customer receives the list

Scenario: Merchant requests orders
	Given a clean bank
	And a registered customer with one unused token 
	And a registered merchant with a bank account
	And the customer has a bank account
	When the merchant scans the token
	And requests payment for 10 kroner using the token
	And the payment succeeds
	And the money is transferred from the customer bank account to the merchant bank account
	Then the merchant requests a semi-anonymous list of sales
	And the merchant receives the list

