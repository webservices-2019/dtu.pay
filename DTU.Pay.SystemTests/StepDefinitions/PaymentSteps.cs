// ***********************************************************************
// Assembly         : DTU.Pay.SystemTests
// Author           : Mads (Lasse)
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="PaymentSteps.cs" company="DTU">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTU.Pay.API.Model;
using DTU.Pay.BankClient;
using DTU.Pay.Clients;
using DTU.Pay.Domain;
using TechTalk.SpecFlow;
using TestHelper;
using Xunit;
using Account = DTU.Pay.BankClient.Model.Account;

//using DTU.Pay.Customer.API.Application;

namespace DTU.Pay.SystemTests.StepDefinitions
{
    [Binding]
    public class PaymentSteps : TestBase
    {
        private CustomerClient _customerClient;
        private MerchantClient _merchantClient;
        private User _customer;
        private User _merchant;
        private TokenModel _customerToken;
        private BankServiceClientAdapter _bankServiceClientAdapter;
        private int _price;
        Order _result;

        private String _firstName;
        private String _lastName;
        private CPRNumber _cpr;

        /// <inheritdoc />
        public PaymentSteps()
        {
            //FakeBus fakeBus = new FakeBus();
            //Storage.Token.Program.Run(fakeBus);
            //Storage.User.Program.Run(fakeBus);

            //Dependencies
            // User
            // User.Storage
            // Account
            // Merchant client
            // Customer client
            // Order
            // Order.Storage
            // Payment

            _customerClient = new CustomerClient("http://localhost/api");
            _merchantClient = new MerchantClient("http://localhost/api");
            _bankServiceClientAdapter = new BankServiceClientAdapter();
            _customer = new User(GenerateCPRNumber(), "lars", "olsen");
            _merchant = new User(GenerateCPRNumber(), "bent", "olsen");
        }

        private async Task SanitizeBank()
        {
            try
            {
                Account account = await _bankServiceClientAdapter.GetAccountByCprNumberAsync(_merchant.Cpr);
                await _bankServiceClientAdapter.RetireAccountAsync(account.AccountID);
            }
            catch
            {
                // ignored
            }

            try
            {
                Account account = await _bankServiceClientAdapter.GetAccountByCprNumberAsync(_customer.Cpr);
                await _bankServiceClientAdapter.RetireAccountAsync(account.AccountID);
            }
            catch
            {
                // ignored
            }
        }

        [Given(@"a clean bank")]
        public async Task GivenACleanBank()
        {
            // SANITIZE BANK
            await SanitizeBank();
        }

        //User
        [Given(@"a first name ""(.*)""")]
        public void GivenAFirstName(string p0)
        {
            //ScenarioContext.Current.Pending();

            _firstName = p0;
        }

        [Given(@"a last name ""(.*)""")]
        public void GivenALastName(string p0)
        {
            //ScenarioContext.Current.Pending();

            _lastName = p0;
        }

        [Given(@"a cprNumber ""(.*)""")]
        public void GivenACprNumber(string p0)
        {
            //ScenarioContext.Current.Pending();

            _cpr = new CPRNumber(p0);
        }
        
        [Given(@"a registered user")]
        public void GivenARegisteredUser()
        {
            //ScenarioContext.Current.Pending();

            //_customer = GenerateUser();
        }

        [When(@"registering the user")]
        public async void WhenRegisteringTheUser()
        {
            //ScenarioContext.Current.Pending();

            _customer = new User(_cpr, _firstName, _lastName);

            await _customerClient.CreateUserAsync(_customer, 0);
        }

        [When(@"the user registers")]
        public async void WhenTheUserRegisters()
        {
            //ScenarioContext.Current.Pending();

            await _customerClient.CreateUserAsync(_customer, 0);
        }

        [When(@"the user requests a deletion")]
        public void WhenTheUserRequestsADeletion()
        {
            //ScenarioContext.Current.Pending();

            _customerClient.DeleteUserAsync(_customer);
        }

        [Then(@"the user is not registered")]
        public void ThenTheUserIsNotRegistered()
        {
            return;

            //ScenarioContext.Current.Pending();
        }

        [Then(@"the user is deleted")]
        public void ThenTheUserIsDeleted()
        {
            //ScenarioContext.Current.Pending();
            return;

            User outUser;

            //Assert.False(_userStorage.TryFetchUserByCpr(_customer.Cpr, out outUser));
        }

        [Then(@"the user is successfully registered")]
        public void ThenTheUserIsSuccessfullyRegistered()
        {
            //ScenarioContext.Current.Pending();
            return; 

            User outUser;

            //Assert.True(_userStorage.TryFetchUserByCpr(_customer.Cpr, out outUser));

        }

        //Payment
        [Given(@"a registered customer with one unused token")]
        public async Task GivenARegisteredCustomerWithOneUnusedToken()
        {
            await _customerClient.CreateUserAsync(_customer, 10000);

            TokensModel tokenModelRests = await (_customerClient.CreateTokensAsync(_customer.Cpr, 1));
            Assert.NotNull(tokenModelRests.Tokens);
            Assert.Single(tokenModelRests.Tokens);
            _customerToken = tokenModelRests.Tokens.First();
        }
        
        [Given(@"a registered merchant with a bank account")]
        public async Task GivenARegisteredMerchantWithABankAccount()
        {
            bool b = await (_merchantClient.CreateUserAsync(_merchant, 10000));
            Assert.True(b);
        }

        [Given(@"the customer has a bank account")]
        public async Task GivenTheCustomerHasABankAccount()
        {
            Account account = await (_bankServiceClientAdapter.GetAccountByCprNumberAsync(_customer.Cpr));
            Assert.NotNull(account);
        }

        [When(@"the merchant scans the token")]
        public void WhenTheMerchantScansTheToken()
        {
            // This step is implicit
            Assert.True(true);
        }
        
        [When(@"requests payment for (.*) kroner using the token")]
        public async Task WhenRequestsPaymentForKronerUsingTheToken(int price)
        {
            _price = price;
            _result = await (_merchantClient.CreateOrderAsync(_merchant.Cpr, _customerToken.Id, "Test order", price));
            
        }
        
        [When(@"the payment succeeds")]
        public void WhenThePaymentSucceeds()
        {
            Assert.NotNull(_result);
            Assert.Equal(_merchant.Cpr, _result.MerchantCPR);
        }
        
        [When(@"the money is transferred from the customer bank account to the merchant bank account")]
        public async Task WhenTheMoneyIsTransferredFromTheCustomerBankAccountToTheMerchantBankAccount()
        {
            Account customerAccount = await (_bankServiceClientAdapter.GetAccountByCprNumberAsync(_customer.Cpr));
            Account merchantAccount = await (_bankServiceClientAdapter.GetAccountByCprNumberAsync(_merchant.Cpr));

            Assert.Equal(10000 - _price, customerAccount.Balance.Value);
            Assert.Equal(10000 + _price, merchantAccount.Balance.Value);
        }
        
        [Then(@"the payment succeeds")]
        public void ThenThePaymentSucceeds()
        {
            Assert.NotNull(_result);
            Assert.Equal(_merchant.Cpr, _result.MerchantCPR);
        }

        [Then(@"the payment fails")]
        public void ThenThePaymentFails()
        {
            Assert.True(true);
        }

        [Then(@"the money is transferred from the customer bank account to the merchant bank account")]
        public async Task ThenTheMoneyIsTransferredFromTheCustomerBankAccountToTheMerchantBankAccount()
        {
            Account customerAccount = await (_bankServiceClientAdapter.GetAccountByCprNumberAsync(_customer.Cpr));
            Account merchantAccount = await (_bankServiceClientAdapter.GetAccountByCprNumberAsync(_merchant.Cpr));

            Assert.Equal(10000 - _price, customerAccount.Balance.Value);
            Assert.Equal(10000 + _price, merchantAccount.Balance.Value);
        }
        
        [Then(@"the money is transferred from the merchant bank account to the customer bank account")]
        public void ThenTheMoneyIsTransferredFromTheMerchantBankAccountToTheCustomerBankAccount()
        {
            Assert.True(true);
        }

        [Then(@"the customer requests a refund")]
        public void ThenTheCustomerRequestsARefund()
        {
            Assert.True(true);
        }

        [Then(@"the customer requests a list of his orders")]
        public void ThenTheCustomerRequestsAListOfHisOrders()
        {
            Assert.True(true);
        }
        
        [Then(@"the customer receives the list")]
        public void ThenTheCustomerReceivesTheList()
        {
            Assert.True(true);
        }
        
        [Then(@"the merchant requests a semi-anonymous list of sales")]
        public void ThenTheMerchantRequestsASemi_AnonymousListOfSales()
        {
            Assert.True(true);
        }
        
        [Then(@"the merchant receives the list")]
        public void ThenTheMerchantReceivesTheList()
        {
            Assert.True(true);
        }
    }
}
