﻿// ***********************************************************************
// Assembly         : DTU.Pay.SystemTests
// Author           : Kris
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="UserSteps.cs" company="DTU">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using DTU.Pay.BankClient;
using DTU.Pay.Clients;
using DTU.Pay.Domain;
using DTU.Pay.Storage;
using System;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TestHelper;
using Xunit;
using AccountA = DTU.Pay.BankClient.Model.Account;


namespace DTU.Pay.SystemTests.StepDefinitions
{
    [Binding]
    public class UserSteps : TestBase
    {
        /*
        private String _firstName;
        private String _lastName;
        private CPRNumber _cpr;

        private User _user;
        private IUserStorage _userStorage;

        private CustomerClient _customerClient;
        private BankServiceClientAdapter _bankServiceClientAdapter;

        public UserSteps()
        {
            //FakeBus fakeBus = new FakeBus();
            //Storage.Token.Program.Run(fakeBus);
            //Storage.User.Program.Run(fakeBus);

            //Dependencies
            // User
            // User.Storage
            // Account
            // Merchant client
            // Customer client
            // Order
            // Order.Storage
            // Payment

            _customerClient = new CustomerClient("http://localhost/api");
            _bankServiceClientAdapter = new BankServiceClientAdapter();
            _userStorage = new UserStorage();
        }

        private async Task SanitizeBank()
        {
            try
            {
                AccountA account = await _bankServiceClientAdapter.GetAccountByCprNumberAsync(_user.Cpr);
                await _bankServiceClientAdapter.RetireAccountAsync(account.AccountID);
            }
            catch
            {
                // ignored
            }
        }

        [Given(@"an empty bank")]
        public async Task GivenAnEmptyBank()
        {
            // SANITIZE BANK
            await SanitizeBank();
        }

        [Given(@"a first name ""(.*)""")]
        public void GivenAFirstName(string p0)
        {
            //ScenarioContext.Current.Pending();

            _firstName = p0;
        }
        
        [Given(@"a last name ""(.*)""")]
        public void GivenALastName(string p0)
        {
            //ScenarioContext.Current.Pending();

            _lastName = p0;
        }
        
        [Given(@"a cprNumber ""(.*)""")]
        public void GivenACprNumber(string p0)
        {
            //ScenarioContext.Current.Pending();

            _cpr = new CPRNumber(p0);
        }
        
        [Given(@"a registered user")]
        public void GivenARegisteredUser()
        {
            //ScenarioContext.Current.Pending();

            _user = GenerateUser();
        }
        
        [When(@"registering the user")]
        public async void WhenRegisteringTheUser()
        {
            //ScenarioContext.Current.Pending();

            _user = new User(_cpr, _firstName, _lastName);

            await _customerClient.CreateUserAsync(_user, 0);
        }
        
        [When(@"the user registers")]
        public async void WhenTheUserRegisters()
        {
            //ScenarioContext.Current.Pending();

            await _customerClient.CreateUserAsync(_user, 0);
        }

        [When(@"the user requests a deletion")]
        public void WhenTheUserRequestsADeletion()
        {
            //ScenarioContext.Current.Pending();

            _customerClient.DeleteUserAsync(_user);
        }
        
        [Then(@"the user is not registered")]
        public void ThenTheUserIsNotRegistered()
        {
            return;

            //ScenarioContext.Current.Pending();
        }
        
        [Then(@"the user is deleted")]
        public void ThenTheUserIsDeleted()
        {
            //ScenarioContext.Current.Pending();

            User outUser;

            Assert.False(_userStorage.TryFetchUserByCpr(_user.Cpr, out outUser));
        }
        
        [Then(@"the user is successfully registered")]
        public void ThenTheUserIsSuccessfullyRegistered()
        {
            //ScenarioContext.Current.Pending();

            User outUser;

            Assert.True(_userStorage.TryFetchUserByCpr(_user.Cpr, out outUser));

        }
        */
    }
}
