﻿// ***********************************************************************
// Assembly         : Payment
// Author           : Mikkel
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="Program.cs" company="Payment">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Globalization;
using System.Threading.Tasks;
using DTU.Pay.BankClient;
using DTU.Pay.BankClient.Model;
using DTU.Pay.Domain.RabbitMQ;
using EasyNetQ;

namespace DTU.Pay.Services.Payment
{
    public class Program
    {
        private static void Main()
        {
            Console.WriteLine("Starting: Payment");

            var bankClient = new BankServiceClientAdapter();

            IBus bus = RabbitHutch.CreateBus(SharedResources.RabbitMQHost);
            bus.RespondAsync<PaymentRequest, PaymentResponse>(async s => await HandlePayment(s, bankClient));
        }

        /// <summary>Tells SOAP Bank to transfer money between two accounts.</summary>
        /// <param name="obj"></param>
        /// <param name="bankClient"></param>
        public static async Task<PaymentResponse> HandlePayment(PaymentRequest obj, BankServiceClientAdapter bankClient)
        {
            Console.WriteLine("Transfer {0} from {1} to {2}", obj.Amount.ToString(NumberFormatInfo.InvariantInfo), obj.CprCustomer.ToString(), obj.CprMerchant.ToString());
            Account merchantAccount = await bankClient.GetAccountByCprNumberAsync(obj.CprMerchant);
            Account customerAccount = await bankClient.GetAccountByCprNumberAsync(obj.CprCustomer);
            await bankClient.TransferMoneyFromToAsync(customerAccount.AccountID, merchantAccount.AccountID, obj.Amount, obj.Description);
            return new PaymentResponse { Status = true };
        }
    }
}