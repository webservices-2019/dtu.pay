// ***********************************************************************
// Assembly         : Payment.Tests
// Author           : Mikkel
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="PaymentTests.cs" company="DTU">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Linq;
using System.Threading.Tasks;
using DTU.Pay.BankClient;
using DTU.Pay.BankClient.Model;
using DTU.Pay.Domain;
using DTU.Pay.Domain.RabbitMQ;
using TestHelper;
using Xunit;
using DTU.Pay.Services.Payment;
using Account = DTU.Pay.BankClient.Model.Account;

namespace Payment.Tests
{
    public class PaymentTests : TestBase
    {
        private BankServiceClientAdapter _bankServiceClientAdapter;

        /// <inheritdoc />
        public PaymentTests()
        {
            _bankServiceClientAdapter = new BankServiceClientAdapter();
        }

        /// <summary>Test whether we can transfer money between two accounts.</summary>
        [Fact]
        public async Task TestTransaction()
        {
            User customer = GenerateUser();
            User merchant = GenerateUser();

            // make sure to delete old users
            try
            {
                Account runSync = await(_bankServiceClientAdapter.GetAccountByCprNumberAsync(customer.Cpr));
                await(_bankServiceClientAdapter.RetireAccountAsync(runSync.AccountID));
            }
            catch (Exception)
            {
                // ignored
            }

            // make sure to delete old users
            try
            {
                Account account = await(_bankServiceClientAdapter.GetAccountByCprNumberAsync(merchant.Cpr));
                await(_bankServiceClientAdapter.RetireAccountAsync(account.AccountID));
            }
            catch (Exception)
            {
                // ignored
            }

            AccountID customerAccountID = await(_bankServiceClientAdapter.CreateAccountWithBalanceAsync(customer, 100));

            AccountID merchantAccountID = await(_bankServiceClientAdapter.CreateAccountWithBalanceAsync(merchant, 100));

            PaymentRequest paymentRequest = new PaymentRequest(merchant.Cpr, customer.Cpr, 10, "Test");
            PaymentResponse paymentResponse = await(Program.HandlePayment(paymentRequest, _bankServiceClientAdapter));
            Assert.True(paymentResponse.Status);

            Account account1 = await(_bankServiceClientAdapter.GetAccountAsync(customerAccountID));
            Assert.Equal(90, account1.Balance.Value);
            Transaction transaction1 = account1.Transactions.First();
            Assert.Equal(10, transaction1.Amount);
            Assert.Equal("Test", transaction1.Discription);

            Account account2 = await(_bankServiceClientAdapter.GetAccountAsync(merchantAccountID));
            Assert.Equal(110, account2.Balance.Value);
            Transaction transaction2 = account2.Transactions.First();
            Assert.Equal(10, transaction2.Amount);
            Assert.Equal("Test", transaction2.Discription);

            await(_bankServiceClientAdapter.RetireAccountAsync(customerAccountID));
            await(_bankServiceClientAdapter.RetireAccountAsync(merchantAccountID));
        }
    }
}