﻿// ***********************************************************************
// Assembly         : Order
// Author           : Rasmus (Lasse)
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="OrderStorage.cs" company="Order">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using DTU.Pay.Domain;

namespace DTU.Pay.Storage
{
    public class OrderStorage : IOrderStorage
    {
        private readonly Dictionary<CPRNumber, List<Order>> _orderStorage;

        public OrderStorage()
        {
            _orderStorage = new Dictionary<CPRNumber, List<Order>>();
        }

        /// <summary>Adds order to the "Order DB"</summary>
        /// <param name="order"></param>
        public void AddOrder(Order order)
        {
            if (_orderStorage.TryGetValue(order.CustomerCPR, out var orders))
            {
                orders.Add(order);
            }
            else
            {
                _orderStorage.Add(order.CustomerCPR, new List<Order> { order });
            }

            if (_orderStorage.TryGetValue(order.MerchantCPR, out orders))
            {
                orders.Add(order);
            }
            else
            {
                _orderStorage.Add(order.MerchantCPR, new List<Order> { order });
            }
        }

        /// <summary>Returns a list of orders based on CPRnumber from the "Order DB"</summary>
        /// <param name="merchantCprNumber"></param>
        /// <param name="order"></param>
        public bool TryFetchOrders(CPRNumber merchantCprNumber, out List<Order> order)
        {
            return _orderStorage.TryGetValue(merchantCprNumber, out order);
        }
    }
}