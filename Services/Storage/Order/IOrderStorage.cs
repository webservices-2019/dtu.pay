﻿// ***********************************************************************
// Assembly         : Order
// Author           : Rasmus
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="IOrderStorage.cs" company="Order">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using DTU.Pay.Domain;

namespace DTU.Pay.Storage
{
    public interface IOrderStorage
    {
        void AddOrder(Order order);
        bool TryFetchOrders(CPRNumber merchantCprNumber, out List<Order> order);
    }
}