﻿// ***********************************************************************
// Assembly         : Order
// Author           : Rasmus (Lasse)
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="Program.cs" company="Order">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using DTU.Pay.Domain;
using EasyNetQ;

namespace DTU.Pay.Storage
{
    internal static class Program
    {
        private static OrderStorage _storage;

        private static void Main()
        {
            // Connect to rabbitMQ
            IBus bus = RabbitHutch.CreateBus(SharedResources.RabbitMQHost);

            _storage = new OrderStorage();
            Run(bus);
        }

        private static void Run(IBus bus)
        {
            Console.WriteLine("Starting: Tokens Storage");
            bus.Receive<CreateOrderRequest>(nameof(CreateOrderRequest), request => StoreOrder(request));
            bus.Respond<OrderFetchRequest, OrderFetchResponse>(HandleMerchantOrderFetchRequest);
        }

        /// <summary>
        ///   <para>Returns a list of orders based on CPR number.</para>
        /// </summary>
        /// <param name="request"></param>
        private static OrderFetchResponse HandleMerchantOrderFetchRequest(OrderFetchRequest request)
        {
            if (_storage.TryFetchOrders(request.Cpr, out List<Order> order))
                return new OrderFetchResponse(order);

            return new OrderFetchResponse(new List<Order>());
        }

        /// <summary>Stores an order in the "Order DB"</summary>
        /// <param name="createOrderRequest"></param>
        private static void StoreOrder(CreateOrderRequest createOrderRequest)
        {
            Console.WriteLine("Store order {0} from {1} to {2}", createOrderRequest.Order.ItemObj.Price.ToString(), createOrderRequest.Order.CustomerCPR.ToString(), createOrderRequest.Order.MerchantCPR.ToString());

            _storage.AddOrder(createOrderRequest.Order);
        }
    }
}