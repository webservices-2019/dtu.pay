// ***********************************************************************
// Assembly         : Order.Tests
// Author           : Rasmus 
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="OrderStorageTests.cs" company="DTU">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using System.Linq;
using DTU.Pay.Domain;
using TestHelper;
using Xunit;

namespace DTU.Pay.Storage
{
    public class OrderStorageTests : TestBase
    {
        private readonly IOrderStorage _orderStorage;

        /// <inheritdoc />
        public OrderStorageTests()
        {
            _orderStorage = new OrderStorage();
        }

        [Fact]
        public void StoreAndRetrieveTest()
        {
            Order order = GenerateOrder();
            _orderStorage.AddOrder(order);

            bool found = _orderStorage.TryFetchOrders(order.CustomerCPR, out List<Order> orders);
            Assert.True(found);
            Assert.Single(orders);
            Assert.Equal(order.ItemObj.Price, orders.First().ItemObj.Price);

            found = _orderStorage.TryFetchOrders(order.MerchantCPR, out orders);
            Assert.True(found);
            Assert.Single(orders);
            Assert.Equal(order.ItemObj.Price, orders.First().ItemObj.Price);

            Order temp = GenerateOrder();
            _orderStorage.AddOrder(temp);

            temp = GenerateOrder();
            temp.CustomerCPR = order.CustomerCPR;
            _orderStorage.AddOrder(temp);

            temp = GenerateOrder();
            _orderStorage.AddOrder(temp);

            found = _orderStorage.TryFetchOrders(order.CustomerCPR, out orders);
            Assert.True(found);
            Assert.Equal(2, orders.Count);

            found = _orderStorage.TryFetchOrders(order.MerchantCPR, out orders);
            Assert.True(found);
            Assert.Single(orders);
            Assert.Equal(order.ItemObj.Price, orders.First().ItemObj.Price);
        }
    }
}