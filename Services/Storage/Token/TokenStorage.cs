﻿// ***********************************************************************
// Assembly         : Token
// Author           : Casper (Lasse, rasmus)
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="TokenStorage.cs" company="Token">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using DTU.Pay.Domain;

namespace DTU.Pay.Storage
{
    public class TokenStorage : ITokenStorage
    {
        private readonly Dictionary<TokenID, Token> _tokenStorage;

        /// <inheritdoc />
        public TokenStorage()
        {
            _tokenStorage = new Dictionary<TokenID, Token>();
        }

        /// <summary>Adds a token o bj to the "Token DB"</summary>
        /// <param name="token"></param>
        /// <inheritdoc />
        public void StoreToken(Token token)
        {
            _tokenStorage.Add(token.Id, token);
        }

        /// <summary>
        ///   <para>Returns a token obj based on tokenID from the "Token DB"</para>
        /// </summary>
        /// <param name="tokenId"></param>
        /// <param name="token"></param>
        /// <inheritdoc />
        public bool TryFetchToken(TokenID tokenId, out Token token)
        {
            return _tokenStorage.TryGetValue(tokenId, out token);
        }
    }
}