﻿// ***********************************************************************
// Assembly         : Token
// Author           : Casper (Lasse)
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="Program.cs" company="Token">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using DTU.Pay.Domain;
using DTU.Pay.Domain.RabbitMQ;
using EasyNetQ;

namespace DTU.Pay.Storage
{
    public static class Program
    {
        private static TokenStorage _storage;

        private static void Main()
        {
            _storage = new TokenStorage();
            var bus = RabbitHutch.CreateBus(SharedResources.RabbitMQHost);

            Run(bus);
        }

        private static void Run(IBus bus)
        {
            Console.WriteLine("Starting: Token Storage");

            bus.Receive<TokenStorageStoreRequest>(nameof(TokenStorageStoreRequest), request => _storage.StoreToken(request.Token));
            bus.Respond<TokenFetchRequest, TokenFetchResponse>(GetTokenHandler);
        }

        /// <summary>Returns a token obj based on tokenID</summary>
        /// <param name="request"></param>
        private static TokenFetchResponse GetTokenHandler(TokenFetchRequest request)
        {
            TokenFetchResponse tokenFetchResponse = new TokenFetchResponse();

            if (_storage.TryFetchToken(request.TokenId, out Token token))
                tokenFetchResponse.Token = token;

            return tokenFetchResponse;
        }
    }
}