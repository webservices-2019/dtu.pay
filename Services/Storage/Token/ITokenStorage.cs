﻿// ***********************************************************************
// Assembly         : Token
// Author           : Casper (Lasse)
//
// Last Modified By : razze
// Last Modified On : 01-21-2019
// ***********************************************************************
// <copyright file="ITokenStorage.cs" company="Token">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using DTU.Pay.Domain;

namespace DTU.Pay.Storage
{
    public interface ITokenStorage
    {
        void StoreToken(Token token);
        bool TryFetchToken(TokenID tokenId, out Token token);
    }
}