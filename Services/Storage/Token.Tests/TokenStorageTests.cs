// ***********************************************************************
// Assembly         : Token.Tests
// Author           : Rasmus 
//
// Last Modified By : razze
// Last Modified On : 01-21-2019
// ***********************************************************************
// <copyright file="TokenStorageTests.cs" company="DTU">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using DTU.Pay.Domain;
using TestHelper;
using Xunit;

namespace DTU.Pay.Storage
{
    public class TokenStorageTests : TestBase
    {
        private readonly ITokenStorage _tokenStorage;

        /// <inheritdoc />
        public TokenStorageTests()
        {
            _tokenStorage = new TokenStorage();
        }

        [Fact]
        public void StoreAndRetrieveTest()
        {
            Token token = GenerateToken();

            Token usedToken = GenerateToken();
            usedToken.Used = true;
            _tokenStorage.StoreToken(usedToken);
            _tokenStorage.StoreToken(GenerateToken());
            _tokenStorage.StoreToken(token);
            _tokenStorage.StoreToken(GenerateToken());
            _tokenStorage.StoreToken(GenerateToken());
            _tokenStorage.StoreToken(GenerateToken());

            Assert.True(_tokenStorage.TryFetchToken(token.Id, out Token storedToken));
            Assert.False(storedToken.Used);

            Assert.True(_tokenStorage.TryFetchToken(usedToken.Id, out Token storedToken2));
            Assert.True(storedToken2.Used);
        }
    }
}