// ***********************************************************************
// Assembly         : User.Tests
// Author           : Casper (Lasse)
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="UserStorageTests.cs" company="DTU">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using DTU.Pay.Domain;
using TestHelper;
using Xunit;

namespace DTU.Pay.Storage
{
    public class UserStorageTests : TestBase
    {
        private readonly IUserStorage _userStorage;

        /// <inheritdoc />
        public UserStorageTests()
        {
            _userStorage = new UserStorage();
        }

        /// <summary>Tests whether we can store a user in the "User DB" and retrieve the it.</summary>
        [Fact]
        public void StoreAndRetrieveTest()
        {
            User user = GenerateUser();
            TokenID token = GenerateTokenID();
            user.Tokens = new List<TokenID>
            {
                GenerateTokenID(),
                GenerateTokenID(),
                token,
                GenerateTokenID(),
                GenerateTokenID()
            };

            _userStorage.StoreUser(user);
            _userStorage.StoreUser(GenerateUser());
            _userStorage.StoreUser(GenerateUser());
            _userStorage.StoreUser(GenerateUser());
            _userStorage.StoreUser(GenerateUser());
            _userStorage.StoreUser(GenerateUser());

            Assert.True(_userStorage.TryFetchUserByCpr(user.Cpr, out User storedUser));
            Assert.Equal(user.FirstName, storedUser.FirstName);
            Assert.Equal(user.LastName, storedUser.LastName);
            Assert.Equal(user.Cpr.Value, storedUser.Cpr.Value);
            Assert.Equal(5, storedUser.Tokens.Count);

            Assert.True(_userStorage.TryFetchUserByToken(token, out User storedUser2));
            Assert.Equal(user.FirstName, storedUser2.FirstName);
            Assert.Equal(user.LastName, storedUser2.LastName);
            Assert.Equal(user.Cpr.Value, storedUser2.Cpr.Value);
            Assert.Equal(5, storedUser2.Tokens.Count);
        }
    }
}