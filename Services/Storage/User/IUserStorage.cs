﻿// ***********************************************************************
// Assembly         : User
// Author           : Lasse
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="IUserStorage.cs" company="User">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using DTU.Pay.Domain;

namespace DTU.Pay.Storage
{
    public interface IUserStorage
    {
        void StoreUser(User user);
        void DeleteUser(CPRNumber cprNumber);
        void UpdateUser(CPRNumber cprNumber, User user);
        void AddUserToken(CPRNumber cprNumber, TokenID tokenId);
        bool TryFetchUserByCpr(CPRNumber cprNumber, out User user);
        bool TryFetchUserByToken(TokenID usrToken, out User user);
        void RemoveUsedToken(TokenID usrToken, CPRNumber cprNumber);
    }
}