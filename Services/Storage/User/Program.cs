﻿// ***********************************************************************
// Assembly         : User
// Author           : Lasse (Rasmus)
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="Program.cs" company="User">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using DTU.Pay.Domain;
using DTU.Pay.Domain.RabbitMQ;
using EasyNetQ;

namespace DTU.Pay.Storage
{
    public static class Program
    {
        private static IUserStorage _storage;

        private static void Main()
        {
            IBus bus = RabbitHutch.CreateBus(SharedResources.RabbitMQHost);

            Run(bus);
        }

        public static void Run(IBus bus)
        {
            Console.WriteLine("Starting: User Storage");
            _storage = new UserStorage();

            bus.Receive<UserStoreRequest>(nameof(UserStoreRequest), request => OnUserStoreRequest(request));
            bus.Receive<UserUpdateRequest>(nameof(UserUpdateRequest), request => OnUserUpdateRequest(request));
            bus.Receive<UserAddTokenRequest>(nameof(UserAddTokenRequest), request => UserAddTokenRequestHandler(request));
            bus.Receive<DeleteAccountRequest>(nameof(DeleteAccountRequest), request => DeleteUser(request));
            bus.Receive<UserRemoveUsedTokenRequest>(nameof(UserRemoveUsedTokenRequest), request => RemoveUsedToken(request));

            bus.Respond<UserCprRequest, UserResponse>(CPRRequestHandler);
            bus.Respond<UserTokenRequest, UserResponse>(UserTokenRequestHandler);
        }

        /// <summary>Remove a token based on tokenID and CPR</summary>
        /// <param name="request"></param>
        private static void RemoveUsedToken(UserRemoveUsedTokenRequest request)
        {
            _storage.RemoveUsedToken(request.tokenID, request.cpr);
        }

        /// <summary>Delete a user based on CPR number.</summary>
        /// <param name="request"></param>
        private static void DeleteUser(DeleteAccountRequest request)
        {
            _storage.DeleteUser(request.cpr);
        }

        /// <summary>Add a token to a user based on tokenID and CPRnumber</summary>
        /// <param name="request"></param>
        private static void UserAddTokenRequestHandler(UserAddTokenRequest request)
        {
            _storage.AddUserToken(request.CprNumber, request.TokenId);
        }

        /// <summary>Updates a user based on CPR and a user obj</summary>
        /// <param name="request"></param>
        private static void OnUserUpdateRequest(UserUpdateRequest request)
        {
            _storage.UpdateUser(request.User.Cpr, request.User);
        }

        /// <summary>Stores a user based on user obj.</summary>
        /// <param name="obj"></param>
        private static void OnUserStoreRequest(UserStoreRequest obj)
        {
            Console.WriteLine("Store user {0} ({1})", obj.User.FirstName, obj.User.Cpr.ToString());

            _storage.StoreUser(obj.User);
        }

        /// <summary>Returns a user based on tokenID</summary>
        /// <param name="request"></param>
        private static UserResponse UserTokenRequestHandler(UserTokenRequest request)
        {
            UserResponse userResponse = new UserResponse();
            if (_storage.TryFetchUserByToken(request.TokenId, out User user))
                userResponse.User = user;

            return userResponse;
        }

        /// <summary>Returns a user based on CPRnumber</summary>
        /// <param name="request"></param>
        private static UserResponse CPRRequestHandler(UserCprRequest request)
        {
            UserResponse userResponse = new UserResponse();

            if (_storage.TryFetchUserByCpr(request.CprNumber, out User user))
                userResponse.User = user;
            return userResponse;
        }
    }
}