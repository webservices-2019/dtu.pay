﻿// ***********************************************************************
// Assembly         : User
// Author           : Lasse
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="UserStorage.cs" company="User">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using DTU.Pay.Domain;

namespace DTU.Pay.Storage
{
    public class UserStorage : IUserStorage
    {
        private readonly Dictionary<CPRNumber, User> _userCprStorage;
        private readonly Dictionary<TokenID, CPRNumber> _userTokenStorage;

        /// <inheritdoc />
        public UserStorage()
        {
            _userCprStorage = new Dictionary<CPRNumber, User>();
            _userTokenStorage = new Dictionary<TokenID, CPRNumber>();
        }

        /// <summary>Add a user obj to the "User DB"</summary>
        /// <param name="user"></param>
        /// <inheritdoc />
        public void StoreUser(User user)
        {
            _userCprStorage.Add(user.Cpr, user);

            foreach (TokenID userToken in user.Tokens)
            {
                _userTokenStorage.Add(userToken, user.Cpr);
            }
        }

        /// <summary>Deletes an user based on CPRNumber from the  "User DB"</summary>
        /// <param name="cprNumber"></param>
        public void DeleteUser(CPRNumber cprNumber)
        {
            if (_userCprStorage.ContainsKey(cprNumber))
            {
                _userCprStorage.Remove(cprNumber);
            }
        }

        /// <summary>Updates an user on the "User DB"</summary>
        /// <param name="cprNumber"></param>
        /// <param name="user"></param>
        /// <inheritdoc />
        public void UpdateUser(CPRNumber cprNumber, User user)
        {
            _userCprStorage[cprNumber] = user;
        }

        /// <summary>
        ///   <para>Adds a token to a user in the "User DB"</para>
        /// </summary>
        /// <param name="cprNumber"></param>
        /// <param name="tokenId"></param>
        /// <inheritdoc />
        public void AddUserToken(CPRNumber cprNumber, TokenID tokenId)
        {
            _userCprStorage[cprNumber].Tokens.Add(tokenId);
            _userTokenStorage.Add(tokenId, cprNumber);
        }

        /// <summary>Returns a user obj based on CPRnumber from "User DB"</summary>
        /// <param name="cprNumber"></param>
        /// <param name="user"></param>
        public bool TryFetchUserByCpr(CPRNumber cprNumber, out User user)
        {
            return _userCprStorage.TryGetValue(cprNumber, out user);
        }

        /// <summary>Returns a user obj based on tokenID from "User DB"</summary>
        /// <param name="usrToken"></param>
        /// <param name="user"></param>
        public bool TryFetchUserByToken(TokenID usrToken, out User user)
        {
            user = null;
            return _userTokenStorage.TryGetValue(usrToken, out CPRNumber cprNumber) && _userCprStorage.TryGetValue(cprNumber, out user);
        }

        /// <summary>Remove a token from a user in "User DB"</summary>
        /// <param name="tokenID"></param>
        /// <param name="cpr"></param>
        public void RemoveUsedToken(TokenID tokenID, CPRNumber cpr)
        {
            _userCprStorage[cpr].Tokens.Remove(tokenID);
        }
    }
}