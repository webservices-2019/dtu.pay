﻿using System;
using System.Collections.Generic;
using System.Linq;
using DTU.Pay.BankClient.Connected_Services.DTU.Pay.BankClient;
using DTU.Pay.Domain;

namespace DTU.Pay.BankClient.Model
{
    public class Account
    {
        public Account(getAccountByCprNumberResponse a)
        {
            AccountID = new AccountID(new Guid(a.@return.id));
            if (a.@return.transactions != null)
                Transactions = a.@return.transactions.Select(transaction => new Transaction(transaction)).ToList();
            else
                Transactions = new List<Transaction>();

            User = new User(new CPRNumber(a.@return.user.cprNumber), a.@return.user.firstName, a.@return.user.lastName);
            if (a.@return.balanceSpecified)
                Balance = a.@return.balance;
        }

        public Account(getAccountResponse a)
        {
            AccountID = new AccountID(new Guid(a.@return.id));
            if (a.@return.transactions != null)
                Transactions = a.@return.transactions.Select(transaction => new Transaction(transaction)).ToList();
            else
                Transactions = new List<Transaction>();

            User = new User(new CPRNumber(a.@return.user.cprNumber), a.@return.user.firstName, a.@return.user.lastName);
            if (a.@return.balanceSpecified)
                Balance = a.@return.balance;
        }

        public List<Transaction> Transactions { get; }
        public AccountID AccountID { get; }
        public User User { get; }
        public decimal? Balance { get; }
    }
}