﻿using System;
using DTU.Pay.BankClient.Connected_Services.DTU.Pay.BankClient;

namespace DTU.Pay.BankClient.Model
{
    public class Transaction
    {
        public Transaction(transaction transaction)
        {
            if (transaction.amountSpecified)
                Amount = transaction.amount;

            if (transaction.balanceSpecified)
                Balance = transaction.balance;
            if (transaction.timeSpecified)
                Time = transaction.time;

            Creditor = transaction.creditor;
            Debitor = transaction.debtor;
            Discription = transaction.description;
        }

        public string Discription { get; set; }

        public string Debitor { get; set; }

        public string Creditor { get; set; }

        public DateTime? Time { get; set; }

        public decimal? Balance { get; }

        public decimal? Amount { get; }
    }
}