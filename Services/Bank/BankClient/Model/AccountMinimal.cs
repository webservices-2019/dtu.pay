﻿using DTU.Pay.Domain;

namespace DTU.Pay.BankClient.Model
{
    public class AccountMinimal
    {
        public AccountID AccountId { get; }
        public User User { get; }

        public AccountMinimal(AccountID accountId, User user)
        {
            AccountId = accountId;
            User = user;
        }
    }
}