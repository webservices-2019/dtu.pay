﻿// ***********************************************************************
// Assembly         : DTU.Pay.BankClient
// Author           : Lasse
//
// Last Modified By : razze
// Last Modified On : 01-22-2019
// ***********************************************************************
// <copyright file="BankServiceClientAdapter.cs" company="DTU.Pay.BankClient">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using DTU.Pay.BankClient.Connected_Services.DTU.Pay.BankClient;
using DTU.Pay.BankClient.Model;
using DTU.Pay.Domain;
using JetBrains.Annotations;
using Account = DTU.Pay.BankClient.Model.Account;

namespace DTU.Pay.BankClient
{
    [PublicAPI]
    public class BankServiceClientAdapter
    {
        private readonly BankServiceClient _bankServiceClient;

        public BankServiceClientAdapter()
        {
            _bankServiceClient = new BankServiceClient();
            _bankServiceClient.Endpoint.Address = new EndpointAddress("http://02267-kolkata.compute.dtu.dk:8080/BankService");
            _bankServiceClient.Endpoint.Binding.OpenTimeout = TimeSpan.FromSeconds(10);
            _bankServiceClient.Endpoint.Binding.SendTimeout = TimeSpan.FromSeconds(10);
            _bankServiceClient.Endpoint.Binding.ReceiveTimeout = TimeSpan.FromSeconds(10);
        }

        public async Task<Account> GetAccountAsync(AccountID accountId)
        {
            return new Account(await _bankServiceClient.getAccountAsync(new getAccount(accountId.Value.ToString())));
        }

        public async Task<Account> GetAccountByCprNumberAsync(CPRNumber cprNumber)
        {
            getAccountByCprNumberResponse getAccountByCprNumberResponse = await _bankServiceClient.getAccountByCprNumberAsync(new getAccountByCprNumber(cprNumber.Value));
            return new Account(getAccountByCprNumberResponse);
        }

        public async Task<AccountID> CreateAccountWithBalanceAsync(User user, decimal balance)
        {
            user userToCreate = new user { cprNumber = user.Cpr.Value, firstName = user.FirstName, lastName = user.LastName };
            createAccountWithBalance request = new createAccountWithBalance(userToCreate, balance);
            createAccountWithBalanceResponse response = await _bankServiceClient.createAccountWithBalanceAsync(request);
            return new AccountID(new Guid(response.@return));
        }

        public async Task<bool> RetireAccountAsync(AccountID accountId)
        {
            retireAccount retireAccount = new retireAccount(accountId.Value.ToString());
            await _bankServiceClient.retireAccountAsync(retireAccount);
            return await Task.FromResult(true);
        }

        public async Task<List<AccountMinimal>> GetAccountsAsync()
        {
            List<AccountMinimal> accountMinimals = new List<AccountMinimal>();
            getAccountsResponse response = await _bankServiceClient.getAccountsAsync(new getAccounts());
            foreach (accountInfo accountInfo in response.@return)
            {
                User user = new User(new CPRNumber(accountInfo.user.cprNumber), accountInfo.user.firstName, accountInfo.user.lastName);
                AccountID accountId = new AccountID(new Guid(accountInfo.accountId));
                AccountMinimal accountMinimal = new AccountMinimal(accountId, user);
                accountMinimals.Add(accountMinimal);
            }

            return accountMinimals;
        }

        public async Task<bool> TransferMoneyFromToAsync(AccountID a1, AccountID a2, decimal amount, string transferDescription)
        {
            transferMoneyFromTo request = new transferMoneyFromTo(a1.ToString(), a2.ToString(), amount, transferDescription);
            await _bankServiceClient.transferMoneyFromToAsync(request);
            return await Task.FromResult(true);
        }
    }
}