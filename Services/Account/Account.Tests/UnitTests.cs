﻿// ***********************************************************************
// Assembly         : Account.Tests
// Author           : Kris
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="UnitTests.cs" company="DTU">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using Xunit;
using DTU.Pay.Domain;
using DTU.Pay.BankClient;
using DTU.Pay.Services.Account;
using TestHelper;
using System;
using System.Threading.Tasks;
using AccountA = DTU.Pay.BankClient.Model.Account;


namespace DTU.Pay.Services.Account.Tests
{
    public class UnitTests : TestBase
    {
        private BankServiceClientAdapter _bankServiceClientAdapter;

        public UnitTests()
        {
            _bankServiceClientAdapter = new BankServiceClientAdapter();
        }

        /// <summary>Tests whether we can create a user and delete the now existing user.</summary>
        [Fact]
        public async Task createAndRemoveUser()
        {
            //Assert.True(true);
            //return;
            User customer = GenerateUser();

            try
            {
                AccountA runSync = await (_bankServiceClientAdapter.GetAccountByCprNumberAsync(customer.Cpr));
                await(_bankServiceClientAdapter.RetireAccountAsync(runSync.AccountID));
            }
            catch (Exception)
            {
                // ignored
            }

           
            CreateAccountRequest CAR = new CreateAccountRequest(customer, 100);
            AccountID AID = (Program.CreateAccount(CAR,_bankServiceClientAdapter).Result);

            Assert.NotNull(AID.GetType());

            DeleteAccountRequest DAR = new DeleteAccountRequest(customer.Cpr);
            bool hasDeleted = (Program.DeleteAccount(DAR,_bankServiceClientAdapter).Result);

            Assert.True(hasDeleted);
            
        }
    }
}