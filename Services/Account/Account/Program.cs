﻿// ***********************************************************************
// Assembly         : Account
// Author           : Kris
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="Program.cs" company="Account">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Threading.Tasks;
using DTU.Pay.BankClient;
using DTU.Pay.Domain;
using DTU.Pay.Domain.RabbitMQ;
using EasyNetQ;
using AccountA = DTU.Pay.BankClient.Model.Account;

namespace DTU.Pay.Services.Account
{
    public class Program
    {
        private static BankServiceClientAdapter _bankClient;

        private static void Main(string[] args)
        {
            Console.WriteLine("Starting: Account");

            IBus bus = RabbitHutch.CreateBus(SharedResources.RabbitMQHost);
            _bankClient = new BankServiceClientAdapter();

            bus.Receive<CreateAccountRequest>(nameof(CreateAccountRequest), async s => await CreateAccount(s, _bankClient));
            bus.Receive<DeleteAccountRequest>(nameof(DeleteAccountRequest), async s => await DeleteAccount(s, _bankClient));

            bus.RespondAsync<ViewAccountRequest, AccountResponse>(async s => await ViewAccount(s));
        }

        /// <summary>Tells SOAP bank to delete account, based on CPRnumber</summary>
        /// <param name="s"></param>
        /// <param name="bc"></param>
        public static async Task<bool> DeleteAccount(DeleteAccountRequest s, BankServiceClientAdapter bc)
        {
            AccountA AID = await bc.GetAccountByCprNumberAsync(s.cpr);
            return await bc.RetireAccountAsync(AID.AccountID);
        }

        /// <summary>Tells SOAP Bank to return an account based on CPRnumber</summary>
        /// <param name="s"></param>
        public static async Task<AccountResponse> ViewAccount(ViewAccountRequest s)
        {
            var account = await _bankClient.GetAccountByCprNumberAsync(s.user.Cpr);
            return new AccountResponse(new Domain.Account(account.AccountID, account.User, account.Balance));
        }

        /// <summary>Tells SOAP Bank to create a new account based on user obj</summary>
        /// <param name="s"></param>
        /// <param name="bc"></param>
        public static async Task<AccountID> CreateAccount(CreateAccountRequest s, BankServiceClientAdapter bc)
        {
            return await bc.CreateAccountWithBalanceAsync(s.user, s.balance);
        }
    }
}