﻿// ***********************************************************************
// Assembly         : Barcode
// Author           : Mads
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="Program.cs" company="Barcode">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Drawing;
using System.Drawing.Imaging;
using DTU.Pay.Domain;
using EasyNetQ;
using QRCoder;

namespace DTU.Pay.Services.Barcode
{
    internal static class Program
    {
        private static QRCodeGenerator _qrGenerator;

        private static void Main()
        {
            Console.WriteLine("Starting: BarcodeProvider");
            _qrGenerator = new QRCodeGenerator();

            // Connect to rabbitMQ
            IBus bus = RabbitHutch.CreateBus(SharedResources.RabbitMQHost);

            // Handle all incoming objects of type BarcodeRequest 
            bus.Receive<BarcodeRequest>(nameof(BarcodeRequest), OnBarcodeRequest);
        }

        private static void OnBarcodeRequest(BarcodeRequest s)
        {
            Console.WriteLine("Generating image.");
            string plainText = s.TokenID.ToString();
            QRCodeData qrCodeData = _qrGenerator.CreateQrCode(plainText, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            qrCodeImage.Save($"/images/{plainText}.jpeg", ImageFormat.Jpeg);
        }
    }
}