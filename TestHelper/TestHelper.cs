﻿// ***********************************************************************
// Assembly         : TestHelper
// Author           : Lasse
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="TestHelper.cs" company="TestHelper">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Threading.Tasks;
using Bogus;
using Bogus.Extensions.Denmark;
using DTU.Pay.Domain;
using JetBrains.Annotations;

namespace TestHelper
{
    [PublicAPI]
    public abstract class TestBase
    {
        public TestBase()
        {
            Randomizer.Seed = new Random(42);
        }

        public CPRNumber GenerateCPRNumber()
        {
            return new CPRNumber(new Faker().Person.Cpr());
        }

        public TokenID GenerateTokenID()
        {
            return new TokenID(new Faker().Random.Guid());
        }

        public AccountID GenerateAccountID()
        {
            return new AccountID(new Faker().Random.Guid());
        }

        public Token GenerateToken()
        {
            return new Token(GenerateTokenID());
        }

        public Order GenerateOrder()
        {
            return new Order(GenerateCPRNumber(), GenerateCPRNumber(), GenerateTokenID(), GenerateItem());
        }

        private Item GenerateItem()
        {
            return new Item(new Faker().Lorem.Sentence(), new Faker().Random.Number(100));
        }

        public User GenerateUser()
        {
            return new User(GenerateCPRNumber(), new Faker().Person.FirstName, new Faker().Person.LastName);
        }
    }
}