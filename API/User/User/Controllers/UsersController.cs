﻿// ***********************************************************************
// Assembly         : User.API
// Author           : Mikkel (Lasse, Rasmus)
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="UsersController.cs" company="User.API">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using DTU.Pay.Domain;
using DTU.Pay.Domain.RabbitMQ;
using EasyNetQ;
using Microsoft.AspNetCore.Mvc;

namespace DTU.Pay.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IBus _bus;

        public UsersController(IBus bus)
        {
            _bus = bus;
        }

        /// <summary>
        ///   <para>Ping request to test the Users API<br />API: "GET <a href="http://ROOTPATH/api/users/ping">http://ROOTPATH/api/users/ping</a>"<br /></para>
        /// </summary>
        [HttpGet]
        [Route("ping")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult HealthCheck()
        {
            return Ok("Pong");
        }

        /// <summary>
        ///   <para>
        /// Returns the list of tokens owned by a given user (Based on CPR)</para>
        ///   <para>API "GET HTTP://ROOTURI/api/users/{cpr}/tokens" </para>
        /// </summary>
        /// <param name="cpr"></param>
        [HttpGet("{cpr}/tokens")]
        public async Task<List<Token>> RetrieveTokens([FromRoute]string cpr)
        {
            UserCprRequest cprRequest = new UserCprRequest
            {
                CprNumber = new CPRNumber(cpr)
            };

            List<Token> tokens = new List<Token>();

            UserResponse responseUser = await _bus.RequestAsync<UserCprRequest, UserResponse>(cprRequest);
            foreach (TokenID responseUserToken in responseUser.User.Tokens)
            {
                TokenFetchResponse foo = await _bus.RequestAsync<TokenFetchRequest, TokenFetchResponse>(new TokenFetchRequest(responseUserToken));
                tokens.Add(foo.Token);
            }

            return tokens;
        }

        /// <summary>
        ///   <para>
        /// Returns a user object. (JSON format)</para>
        ///   <para>API "GET <a href="http://ROOTURI/api/">HTTP://ROOTURI/api/</a>users/{cpr}"</para>
        /// </summary>
        /// <param name="cpr"></param>
        [HttpGet("{cpr}")]
        public async Task<ActionResult<User>> RetrieveUser([FromRoute]string cpr)
        {
            UserCprRequest cprRequest = new UserCprRequest
            {
                CprNumber = new CPRNumber(cpr)
            };

            UserResponse responseUser = await _bus.RequestAsync<UserCprRequest, UserResponse>(cprRequest);
            if (responseUser.User == null)
                return this.ErrorResult("The user was not found.", HttpStatusCode.NotFound);

            return responseUser.User;
        }

        /// <summary>
        ///   <para>
        /// Creates a user in the system</para>
        ///   <para>API "POST <a href="http://ROOTURI/api/">HTTP://ROOTURI/api/</a>users"</para>
        ///   <para>FORM:</para>
        ///   <list type="bullet">
        ///     <item>
        ///       <font color="#2a2a2a">string cpr</font>
        ///     </item>
        ///     <item>
        ///       <font color="#2a2a2a">string fname</font>
        ///     </item>
        ///     <item>
        ///       <font color="#2a2a2a">string lname</font>
        ///     </item>
        ///     <item>
        ///       <font color="#2a2a2a">decimal balance</font>
        ///     </item>
        ///   </list>
        /// </summary>
        /// <param name="cpr"></param>
        /// <param name="fName"></param>
        /// <param name="lName"></param>
        /// <param name="balanceM"></param>
        [HttpPost]
        public async Task MakeUser([FromForm]string cpr, [FromForm]string fName, [FromForm]string lName, [FromForm]decimal balanceM)
        {
            User u = new User(new CPRNumber(cpr), fName, lName);

            CreateAccountRequest accountRequest = new CreateAccountRequest(u, balanceM);

            await _bus.SendAsync(nameof(CreateAccountRequest), accountRequest);
            await _bus.SendAsync(nameof(UserStoreRequest), new UserStoreRequest(u));
        }

        [HttpDelete("{cpr}")]
        public async Task DeleteUser([FromRoute]string cpr)
        {
            DeleteAccountRequest deleteAccountRequest = new DeleteAccountRequest(new CPRNumber(cpr));
            await _bus.SendAsync(nameof(DeleteAccountRequest), deleteAccountRequest);
            await _bus.SendAsync(nameof(DeleteAccountRequest), deleteAccountRequest);

            //await _bus.RequestAsync<DeleteAccountRequest,DeleteAccountResponse>(deleteAccountRequest);
        }
    }
}