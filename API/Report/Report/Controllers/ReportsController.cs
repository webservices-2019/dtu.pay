﻿// ***********************************************************************
// Assembly         : Report.API
// Author           : Lasse
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="ReportsController.cs" company="Report.API">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace DTU.Pay.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        /// <summary>
        ///   <para>Ping request to test the report API<br />API: "GET <a href="http://ROOTPATH/api/reports/ping">http://ROOTPATH/api/reports/ping</a>"<br /></para>
        /// </summary>
        [HttpGet]
        [Route("ping")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult HealthCheck()
        {
            return Ok("Pong");
        }


    }
}