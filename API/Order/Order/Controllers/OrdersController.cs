﻿// ***********************************************************************
// Assembly         : Order.API
// Author           : Rasmus (Lasse, Kris)
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="OrdersController.cs" company="Order.API">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using DTU.Pay.Domain;
using DTU.Pay.Domain.RabbitMQ;
using EasyNetQ;
using Microsoft.AspNetCore.Mvc;

namespace DTU.Pay.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IBus _bus;

        public OrdersController(IBus bus)
        {
            _bus = bus;
        }

        /// <summary>
        ///   <para>Pings the Order API</para>
        ///   <para>API: "GET <a href="http://ROOTPATH/ping&quot;">http://ROOTPATH/ping"</a></para>
        /// </summary>
        [HttpGet]
        [Route("ping")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult HealthCheck()
        {
            return Ok("Pong");
        }


        /// <summary>
        ///   <para>Returns a list of orders</para>
        ///   <para>API: "GET <a href="http://ROOTPATH/api/orders?cpr=00000000000">http://URLPATH/api/orders?cpr=00000000000</a>" </para>
        /// </summary>
        /// <param name="cpr"></param>
        [HttpGet]
        public async Task<List<Order>> GetOrders([FromQuery]string cpr)
        {
            OrderFetchResponse orderFetchResponse = await _bus.RequestAsync<OrderFetchRequest, OrderFetchResponse>(new OrderFetchRequest(new CPRNumber(cpr)));
            return orderFetchResponse.Orders;
        }

        // POST api/values
        /// <summary>
        ///   <para>Creates an order</para>
        ///   <para>API: "POST <a href="http://ROOTPATH/api/orders">http://ROOTPATH/api/orders</a>"</para>
        ///   <para>FORM:</para>
        ///   <list type="bullet">
        ///     <item>string tokenID</item>
        ///     <item>string merchCPR</item>
        ///     <item>int price</item>
        ///     <item>string desc</item>
        ///   </list>
        ///   <para></para>
        /// </summary>
        /// <param name="tokenID"></param>
        /// <param name="merchCPR"></param>
        /// <param name="price"></param>
        /// <param name="desc"></param>
        [HttpPost]
        public async Task<ActionResult<Order>> CreateOrder([FromForm]string tokenID, [FromForm]string merchCPR, [FromForm]int price, [FromForm]string desc)
        {
            TokenID tokenId = new TokenID(tokenID);
            CPRNumber merchantCpr = new CPRNumber(merchCPR);

            //Check if users token is valid
            TokenFetchResponse r = _bus.Request<TokenFetchRequest, TokenFetchResponse>(new TokenFetchRequest(tokenId));

            if (r.Token == null)
                return BadRequest("Token not verified (Missing or used)");



            // Get user based on token
            UserTokenRequest userTokenRequest = new UserTokenRequest(tokenId);

            UserResponse responseUser = await _bus.RequestAsync<UserTokenRequest, UserResponse>(userTokenRequest);

            if (responseUser == null)
                return this.ErrorResult("No user was found to own the token.", HttpStatusCode.InternalServerError);

            // Create order
            Order order = new Order(responseUser.User.Cpr, merchantCpr, tokenId, new Item(desc, price));

            CreateOrderRequest createOrderRequest = new CreateOrderRequest(order);

            // Send payment to queue
            PaymentRequest payReq = new PaymentRequest(order.MerchantCPR, order.CustomerCPR, order.ItemObj.Price, order.ItemObj.Description);

            PaymentResponse payResp = await _bus.RequestAsync<PaymentRequest, PaymentResponse>(payReq);

            await _bus.SendAsync(nameof(UserRemoveUsedTokenRequest), new UserRemoveUsedTokenRequest(tokenId, responseUser.User.Cpr));
            if (payResp.Status)
            {
                await _bus.SendAsync(nameof(CreateOrderRequest), createOrderRequest);
            }
            else
            {
                return this.ErrorResult("Payment failed. Order has not been saved.", HttpStatusCode.InternalServerError);
            }
                
            return Ok(order);
        }
    }
}