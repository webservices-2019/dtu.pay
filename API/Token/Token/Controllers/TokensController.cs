﻿// ***********************************************************************
// Assembly         : Token.API
// Author           : Rasmus (Lasse)
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="TokensController.cs" company="Token.API">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DTU.Pay.API.Model;
using DTU.Pay.Domain;
using DTU.Pay.Domain.RabbitMQ;
using EasyNetQ;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DTU.Pay.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokensController : ControllerBase
    {
        private readonly IBus _bus;
        private readonly ILogger<TokensController> _logger;

        /// <inheritdoc />
        public TokensController(IBus bus, ILogger<TokensController> logger)
        {
            _bus = bus;
            _logger = logger;
        }

        /// <summary>
        ///   <para>Ping request to test the token API<br />API: "GET <a href="http://ROOTPATH/api/tokens/ping">http://ROOTPATH/api/tokens/ping</a>"<br /></para>
        /// </summary>
        [HttpGet]
        [Route("ping")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult HealthCheck()
        {
            return Ok("Pong");
        }

        /// <summary>
        ///   <para>
        /// Returns a list of tokens in json format if user has no more than 1 token.</para>
        ///   <para>API "POST <a href="http://ROOTPATH/api/">http://ROOTPATH/api/</a>tokens"</para>
        ///   <para>FORM:</para>
        ///   <list type="bullet">
        ///     <item>
        ///       <font color="#2a2a2a">string cpr</font>
        ///     </item>
        ///     <item>
        ///       <font color="#2a2a2a">uint amount</font>
        ///     </item>
        ///   </list>
        /// </summary>
        /// <param name="cpr"></param>
        /// <param name="amount"></param>
        /// 
        [HttpPost]
        public async Task<ActionResult<TokensModel>> RequestTokens([FromForm]string cpr, [FromForm]uint amount)
        {
            _logger.LogDebug("RequestTokens: {cpr} | {amount}", cpr, amount);
            UserCprRequest userCprRequest = new UserCprRequest
            {
                CprNumber = new CPRNumber(cpr)
            };

            UserResponse userResponse = await _bus.RequestAsync<UserCprRequest, UserResponse>(userCprRequest);

            if (userResponse?.User == null)
                return this.ErrorResult("The user doesn't exist.", HttpStatusCode.BadRequest);

            if (userResponse.User.Tokens.Count > 1)
                return this.ErrorResult("You already have at least one token.", HttpStatusCode.BadRequest);

            _logger.LogDebug("RequestTokens: Generating tokens.");

            List<Token> tokens = GenerateTokens(amount).ToList();
            foreach (Token token in tokens)
            {
                await _bus.SendAsync(nameof(TokenStorageStoreRequest), new TokenStorageStoreRequest(token));
                await _bus.SendAsync(nameof(BarcodeRequest), new BarcodeRequest(token.Id));
                await _bus.SendAsync(nameof(UserAddTokenRequest), new UserAddTokenRequest(token.Id, userResponse.User.Cpr));
            }

            TokensModel requestTokens = new TokensModel();
            requestTokens.Tokens = tokens.Select(token => TokenModel.FromToken(token)).ToList();

            return requestTokens;
        }

        private IEnumerable<Token> GenerateTokens(uint amount)
        {
            for (int i = 0; i < amount; i++)
            {
                TokenID tokenId = new TokenID(Guid.NewGuid());
                yield return new Token(tokenId);
            }
        }

        /// <summary>
        ///   <para>Test if tokens exists in system. Returns the token object.</para>
        ///   <para>API "GET <a href="http://ROOTURI/">HTTP://ROOTURI/</a>api/tokens/{tokenid}"</para>
        /// </summary>
        /// <param name="tokenId"></param>
        [HttpGet("{tokenId}")]
        public async Task<ActionResult<TokenModel>> GetToken([FromRoute]string tokenId)
        {
            TokenFetchRequest tokenVerificationFetch = new TokenFetchRequest(new TokenID(tokenId));
            TokenFetchResponse tokenVerificationResponse = await _bus.RequestAsync<TokenFetchRequest, TokenFetchResponse>(tokenVerificationFetch);

            if (tokenVerificationResponse?.Token != null)
                return TokenModel.FromToken(tokenVerificationResponse.Token);

            return this.ErrorResult("The token was not found.", HttpStatusCode.BadRequest);
        }

        /// <summary>
        ///   <para>Returns an image of a barcode based on token.</para>
        ///   <para>API "GET HTTP://ROOTURI/api/tokens/{tokenid}/image"</para>
        /// </summary>
        /// <param name="tokenId"></param>
        [HttpGet("{tokenId}/image")]
        public ActionResult GetTokenImage([FromRoute]string tokenId)
        {
            try
            {
                using (FileStream fileStream = System.IO.File.OpenRead($"/images/{tokenId}"))
                    return File(fileStream, "image/jpeg");
            }
            catch (Exception e)
            {
                return this.ErrorResult(e.Message, HttpStatusCode.InternalServerError);
            }
        }
    }
}