﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTU.Pay.Domain
{
    public class DeleteAccountRequest 
    {
        public CPRNumber cpr;

        public DeleteAccountRequest(CPRNumber cpr)
        {
            this.cpr = cpr;
        }

    }
}
