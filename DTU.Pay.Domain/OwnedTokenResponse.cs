﻿//using DTU.Pay.Domain;

using System.Collections.Generic;

namespace DTU.Pay.Domain
{
    public class OwnedTokenResponse
    {
        public List<Token> Tokens { get; set; }
    }
}