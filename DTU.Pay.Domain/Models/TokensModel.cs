﻿using System.Collections.Generic;
using DTU.Pay.Domain;

namespace DTU.Pay.API.Model
{
    public class TokensModel
    {
        public List<TokenModel> Tokens { get; set; }
    }
}