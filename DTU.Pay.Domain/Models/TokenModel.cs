﻿using DTU.Pay.Domain;

namespace DTU.Pay.API.Model
{
    public class TokenModel
    {
        public static TokenModel FromToken(Token token)
        {
            return new TokenModel() { Id = token.Id, Used = token.Used, ImageURL = "http://02267-seoul2.compute.dtu.dk/api/tokens/" + token.Id + "/image" };
        }

        public TokenID Id { get; set; }
        public bool Used { get; set; }
        public string ImageURL { get; set; }
    }
}