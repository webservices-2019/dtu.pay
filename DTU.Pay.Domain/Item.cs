﻿namespace DTU.Pay.Domain
{
    public class Item
    {
        public Item(string description, int price)
        {
            Price = price;
            Description = description;
        }

        public int Price { get; }
        public string Description { get; }
    }
}