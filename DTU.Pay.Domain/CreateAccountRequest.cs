﻿//using DTU.Pay.Domain;

namespace DTU.Pay.Domain
{
    public class CreateAccountRequest
    {
        public User user;
        public decimal balance;

        public CreateAccountRequest(User u, decimal b)
        {
            user = u;
            balance = b;
        }

    }
}