﻿namespace DTU.Pay.Domain
{
    public class Token
    {
        public TokenID Id { get; }
        public bool Used { get; set; }

        public Token(TokenID id)
        {
            Id = id;
        }
    }
}