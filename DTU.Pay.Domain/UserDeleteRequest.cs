﻿namespace DTU.Pay.Domain
{
    public class UserDeleteRequest
    {
        public CPRNumber Cpr { get; }
        public UserDeleteRequest(CPRNumber cpr)
        {
            Cpr = cpr;
        }
    }
}