﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTU.Pay.Domain
{
    public class DeleteAccountResponse
    {
        public bool _status;

        public DeleteAccountResponse(bool status)
        {
            this._status=status;
        }

    }
}
