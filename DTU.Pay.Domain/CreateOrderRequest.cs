﻿namespace DTU.Pay.Domain
{
    public class CreateOrderRequest
    {
        public CreateOrderRequest(Order order)
        {
            Order = order;
        }

        public Order Order { get; }
    }
}