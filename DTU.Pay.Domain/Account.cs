﻿namespace DTU.Pay.Domain
{
    public class Account
    {
        public Account(AccountID accountId, User user, decimal? balance)
        {
            AccountID = accountId;
            User = user;
            Balance = balance;
        }

        public AccountID AccountID { get; }
        public User User { get; }
        public decimal? Balance { get; }
    }
}