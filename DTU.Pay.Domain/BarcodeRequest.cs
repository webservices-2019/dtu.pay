﻿namespace DTU.Pay.Domain
{
    public class BarcodeRequest
    {
        public BarcodeRequest(TokenID tokenId)
        {
            TokenID = tokenId;
        }

        public TokenID TokenID { get; }
    }
}