﻿using System.Collections.Generic;

namespace DTU.Pay.Domain
{
    public class User
    {
        public CPRNumber Cpr { get; }
        public string FirstName { get; }
        public string LastName { get; }

        public User(CPRNumber cpr, string firstName, string lastName)
        {
            Cpr = cpr;
            FirstName = firstName;
            LastName = lastName;
            Tokens = new List<TokenID>();
        }

        public List<TokenID> Tokens { get; set; }
    }
}