﻿namespace DTU.Pay.Domain
{
    public class UserTokenRequest
    {
        public UserTokenRequest(TokenID tokenId)
        {
            TokenId = tokenId;
        }

        //To be defined
        public TokenID TokenId { get; set; }
    }
}