﻿namespace DTU.Pay.Domain
{
    public class OrderFetchRequest
    {
        public OrderFetchRequest(CPRNumber cpr)
        {
            Cpr = cpr;
        }

        public CPRNumber Cpr { get; }
    }
}