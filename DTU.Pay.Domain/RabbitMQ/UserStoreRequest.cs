﻿namespace DTU.Pay.Domain.RabbitMQ
{
    public class UserStoreRequest
    {
        public UserStoreRequest(User user)
        {
            User = user;
        }

        public User User { get; }
    }
}