﻿namespace DTU.Pay.Domain.RabbitMQ
{
    public class TokenStorageStoreRequest
    {
        public TokenStorageStoreRequest(Token token)
        {
            Token = token;
        }

        public Token Token { get; }
    }
}