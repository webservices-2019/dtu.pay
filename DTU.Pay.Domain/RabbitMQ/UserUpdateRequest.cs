﻿namespace DTU.Pay.Domain.RabbitMQ
{
    public class UserUpdateRequest
    {
        /// <inheritdoc />
        public UserUpdateRequest(User user)
        {
            User = user;
        }

        public User User { get; }
    }
}