﻿namespace DTU.Pay.Domain.RabbitMQ
{
    public class UserAddTokenRequest
    {
        /// <inheritdoc />
        public UserAddTokenRequest(TokenID tokenId, CPRNumber cprNumber)
        {
            TokenId = tokenId;
            CprNumber = cprNumber;
        }

        public TokenID TokenId { get; }
        public CPRNumber CprNumber { get; }
    }
}