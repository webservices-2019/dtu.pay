﻿namespace DTU.Pay.Domain.RabbitMQ
{
    public class TokenFetchResponse
    {
        public Token Token { get; set; }
    }
}