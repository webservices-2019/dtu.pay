﻿namespace DTU.Pay.Domain.RabbitMQ
{
    public class TokenVerificationRequest
    {
        public TokenVerificationRequest(TokenID tokenId)
        {
            TokenId = tokenId;
        }

        public TokenID TokenId { get; }
    }
}