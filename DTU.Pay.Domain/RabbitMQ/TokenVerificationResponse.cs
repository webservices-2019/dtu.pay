﻿namespace DTU.Pay.Domain.RabbitMQ
{
    public class TokenVerificationResponse
    {
        public Token Token { get; }

        public TokenVerificationResponse(Token token)
        {
            Token = token;
        }
    }
}