﻿namespace DTU.Pay.Domain.RabbitMQ
{
    public class PaymentResponse
    {
        public bool Status { get; set; }
    }
}