﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTU.Pay.Domain.RabbitMQ
{
    public class UserRemoveUsedTokenRequest
    {
        public UserRemoveUsedTokenRequest(TokenID tokenID, CPRNumber cpr)
        {
            this.tokenID = tokenID;
            this.cpr = cpr;
        }

        public TokenID tokenID {get;}
        public CPRNumber cpr { get; }


    }
}
