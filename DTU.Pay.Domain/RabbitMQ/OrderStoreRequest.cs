﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTU.Pay.Domain.RabbitMQ
{
    public class OrderStoreRequest
    {
        public Order Order { get; }

        public OrderStoreRequest(Order order)
        {
            Order = order;
        }
    }
}
