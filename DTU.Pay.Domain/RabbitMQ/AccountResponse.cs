﻿namespace DTU.Pay.Domain.RabbitMQ
{
    public class AccountResponse
    {
        public Account Account { get; }

        public AccountResponse(Account account)
        {
            Account = account;
        }
    }
}