﻿namespace DTU.Pay.Domain.RabbitMQ
{
    public class TokenFetchRequest
    {
        public TokenFetchRequest(TokenID tokenId)
        {
            TokenId = tokenId;
        }

        public TokenID TokenId { get; }
    }
}