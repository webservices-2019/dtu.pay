﻿namespace DTU.Pay.Domain.RabbitMQ
{
    public class PaymentRequest
    {
        public PaymentRequest(CPRNumber cprMerchant, CPRNumber cprCustomer, decimal amount, string description)
        {
            CprMerchant = cprMerchant;
            CprCustomer = cprCustomer;
            Amount = amount;
            Description = description;
        }

        public CPRNumber CprMerchant { get; set; }
        public CPRNumber CprCustomer { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
    }
}