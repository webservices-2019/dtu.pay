﻿using System.Collections.Generic;

namespace DTU.Pay.Domain
{
    public class OrderFetchResponse
    {
        public OrderFetchResponse(List<Order> orders)
        {
            Orders = orders;
        }

        public List<Order> Orders { get; set; }
    }
}