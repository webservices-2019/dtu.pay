﻿namespace DTU.Pay.Domain
{
    public class TokenUseRequest
    {
        public CPRNumber UserIdentifier { get; set; }
        public TokenID TokenIdentifier { get; set; }
    }
}