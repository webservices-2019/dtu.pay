﻿namespace DTU.Pay.Domain
{
    public class TokenUseResponse
    {
        public bool Successful { get; set; }
    }
}