﻿using System;
using Newtonsoft.Json;

namespace DTU.Pay.Domain
{
    public struct AccountID
    {
        public Guid Value { get; }

        [JsonConstructor]
        public AccountID(Guid value)
        {
            Value = value;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return Value.ToString();
        }

        public bool Equals(AccountID other)
        {
            return Value.Equals(other.Value);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            return obj is AccountID other && Equals(other);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}