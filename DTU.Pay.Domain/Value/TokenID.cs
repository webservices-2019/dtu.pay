﻿using System;
using Newtonsoft.Json;

namespace DTU.Pay.Domain
{
    public struct TokenID
    {
        public Guid Value { get; }

        [JsonConstructor]
        public TokenID(Guid value)
        {
            Value = value;
        }

        public TokenID(string value)
        {
            Value = new Guid(value);
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return Value.ToString();
        }

        public bool Equals(TokenID other)
        {
            return Value.Equals(other.Value);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            return obj is TokenID other && Equals(other);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}