﻿using Newtonsoft.Json;

namespace DTU.Pay.Domain
{
    public struct CPRNumber
    {
        public string Value { get; }

        [JsonConstructor]
        public CPRNumber(string value)
        {
            Value = value;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return Value;
        }

        public bool Equals(CPRNumber other)
        {
            return string.Equals(Value, other.Value);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            return obj is CPRNumber other && Equals(other);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }
    }
}