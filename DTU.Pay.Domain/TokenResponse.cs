﻿using System.Collections.Generic;

namespace DTU.Pay.Domain
{
    public class TokenResponse
    {
        public List<Token> Tokens { get; set; }
    }
}