﻿namespace DTU.Pay.Domain
{
    public class Order
    {
        public CPRNumber CustomerCPR { get; set; }
        public CPRNumber MerchantCPR { get; set; }
        public TokenID TokenID { get; set; }
        public Item ItemObj { get; set; }

        /// <inheritdoc />
        public Order(CPRNumber customerCpr, CPRNumber merchantCpr, TokenID tokenId, Item itemObj)
        {
            CustomerCPR = customerCpr;
            MerchantCPR = merchantCpr;
            TokenID = tokenId;
            ItemObj = itemObj;
        }
    }
}