﻿// ***********************************************************************
// Assembly         : Shared
// Author           : Lasse
//
// Last Modified By : razze
// Last Modified On : 01-23-2019
// ***********************************************************************
// <copyright file="SharedResources.cs" company="DTU">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Net;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DTU.Pay
{
    /// <summary>Resources which are shared across the entire solution.</summary>
    public static class SharedResources
    {
        public const string RabbitMQHost = "host=dtu.pay.rabbitmq;prefetchcount=1;timeout=60";

        public static void ConfigureCommonServices(IServiceCollection services)
        {
            services.RegisterEasyNetQ(RabbitMQHost); // ip of rabbitmq
            services.AddLogging(builder => builder.AddConsole());
        }
    }

    public static class ControllerExtensions
    {
        [MustUseReturnValue]
        public static ActionResult ErrorResult(this ControllerBase controller, string message, HttpStatusCode code)
        {
            return controller.StatusCode((int)code, new ErrorDetails()
            {
                StatusCode = (int)code,
                Message = message
            });
        }
    }

    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }

    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _logger = logger;
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong: {ex}");
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";

            return context.Response.WriteAsync(new ErrorDetails()
            {
                StatusCode = context.Response.StatusCode,
                Message = exception.Message
            }.ToString());
        }
    }

    public class ErrorDetails
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}